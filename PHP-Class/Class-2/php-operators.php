<h1>1.	Arithmetic operators</h1>
<?php
	$x = 10;
	$y = 4;
	echo($x + $y)."<br/>"; // 0utputs: 14
	echo($x - $y)."<br/>"; // 0utputs: 6
	echo($x * $y)."<br/>"; // 0utputs: 40
	echo($x / $y)."<br/>"; // 0utputs: 2.5
	echo($x % $y)."<br/>"; // 0utputs: 2
?>

<h1>2.	Assignment operators</h1>
	<?php

	$x = 10;
	echo $x."<br/>"; // Outputs: 10
	 
	$x = 20;
	$x += 30;
	echo $x."<br/>"; // Outputs: 50
	 
	$x = 50;
	$x -= 20;
	echo $x."<br/>"; // Outputs: 30
	 
	$x = 5;
	$x *= 25;
	echo $x."<br/>"; // Outputs: 125
	 
	$x = 50;
	$x /= 10;
	echo $x."<br/>"; // Outputs: 5
	 
	$x = 100;
	$x %= 15;
	echo $x; // Outputs: 10
	?>

<h1>3.	Comparison operators</h1>
<?php
$x = 25;
$y = 35;
$z = "25";

var_dump($x == $z);  // Outputs: boolean true
echo "<br/>";
var_dump($x === $z); // Outputs: boolean false
echo "<br/>";
var_dump($x != $y);  // Outputs: boolean true
echo "<br/>";
var_dump($x !== $z); // Outputs: boolean true
echo "<br/>";
var_dump($x < $y);   // Outputs: boolean true
echo "<br/>";
var_dump($x > $y);   // Outputs: boolean false
echo "<br/>";
var_dump($x <= $y);  // Outputs: boolean true
echo "<br/>";
var_dump($x >= $y);  // Outputs: boolean false
echo "<br/>";
?>

<h1>4.	Increment/Decrement operators</h1>
	<?php
	$x = 10;
	echo ++$x; // Outputs: 11
	echo $x;   // Outputs: 11
	 
	$x = 10;
	echo $x++; // Outputs: 10
	echo $x;   // Outputs: 11
	 
	$x = 10;
	echo --$x; // Outputs: 9
	echo $x;   // Outputs: 9
	 
	$x = 10;
	echo $x--; // Outputs: 10
	echo $x;   // Outputs: 9
	?>

<h1>5.	Logical operators</h1>

<?php
$year = 2020;
// Leap years are divisible by 400 or by 4 but not 100
if(($year % 400 == 0) || (($year % 100 != 0) && ($year % 4 == 0))){
    echo "$year is a leap year.";
} else{
    echo "$year is not a leap year.";
}
?>


<h1>6.	String operators</h1>
<?php
	$x = "Hello";
	$y = " World!";
	echo $x . $y; // Outputs: Hello World!
	 
	$x .= $y;
	echo $x; // Outputs: Hello World!
?>

<h1>7.	Array operators</h1>


<h1>8.	Conditional assignment operators</h1>


