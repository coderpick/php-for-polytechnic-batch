
<?php 

	$x = 25;
	$y = 30;
	$z = $x*$y;

   echo $z;

?>
<h2>php constants</h2>
<?php 
		
		define("SERVER_NAME", "localhost");
		define("SERVER_USER_NAME", "root");
		define("DB_PASSWORD", "");
		define("DB_NAME", "ecommerce");

		echo SERVER_NAME;


 ?>
<hr>
<h1>php data types</h1>
<hr>
<h3>1.String</h3>
<?php		
		$user_name = "Abdul Karim 555";
        var_dump($user_name);
		
 ?>

<h3>2.integer</h3>
<?php		
		$age = 2545444;
        var_dump($age);
 ?>
<h3>3.Float</h3>
<?php		
		$price = 100.2;
        var_dump($price);
 ?>

 <h3>4.Boolean</h3>
<?php		
		$error = false;
        var_dump($error);
 ?>
  <h3>5.Array</h3>
<?php		
	 	$city = ['dhaka','tangail','natore','khulna',552];
		//$city = array('dhaka','tangail','natore','khulna',552);

		var_dump($city);
		//echo $city[1]
		
 ?>

<h3>6.Object</h3>
<?php	
    
    class User
    {
    	public $userName = "Sojib";
	}

	$user= new User();
	//echo $user->userName;
	var_dump($user);
 ?>
 <h3>7.NULL</h3>
 <?php 
 		$b = 255;


 		$b = null;

 		echo $b;
  ?>
    
     <h3>8.Resource</h3>
     <?php 

     		// $connection = mysqli_connect('localhost','root','');

     		// var_dump($connection)

        $fileOpen = fopen('test.txt', "w");

        var_dump($fileOpen);

      ?>





