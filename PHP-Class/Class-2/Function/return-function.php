<?php


function sum($x, $y) {
	
     $z = $x + $y;
     return $z;
}

echo "5 + 10 = " . sum(5,10) . "<br>";
echo "7 + 13 = " . sum(7,13) . "<br>";
echo "2 + 4 = " . sum(2,4);

echo "<hr/>";

// Defining function
function getSum($num1, $num2){
    $total = $num1 + $num2;
    return $total;
}
 
// Printing returned value
echo "Sum of the two numbers 5 and 10 is :" . getSum(5, 10);
?>