<?php include "inc/header.php";?>

<?php
	if (isset($_POST['submit'])) {

			    $file_name   =  $_FILES["photo"]["name"];			 
			    $file_size 	 =  $_FILES["photo"]["size"];
			    $file_tmp    =  $_FILES["photo"]["tmp_name"];

			     $permited  = array('jpg', 'jpeg', 'png', 'gif'); 

			     $div 	   = explode(".", $file_name);
			     $file_ext = strtolower(end($div));
			     $unique_image = substr(md5(time()), 0, 5).'.'.$file_ext;
			     $uploaded_image = "uploads/".$unique_image;

			     if (empty($file_name)) {
		           echo "<span class='text-danger'>Please Select any Image !</span>";
		          }elseif ($file_size >1048567) {
		           echo "<span class='text-danger'>Image Size should be less then 1MB!
		           </span>";
		          } elseif (in_array($file_ext, $permited) === false) {
		           echo "<span class='text-danger'>You can upload only:-"
		           .implode(', ', $permited)."</span>";
		          } else{

			    move_uploaded_file($file_tmp, $uploaded_image);
          $insert ="INSERT INTO  fileupload(image)VALUES('$uploaded_image')";
          if (mysqli_query($conn,$insert)) {
           echo "<span class='text-success'>file upload successfully</span>";
          }
			   
			}
		
	}
 
?>


      <div class="container">
         <div class="row">
           <div class="col-md-8 col-md-offset-2">
              <div class="panel panel-primary">
                <div class="panel-heading">
                    <h1 class="panel-title">Image Upload</h1>
                </div>
                <div class="panel-body">
                  <div class="myform">
                    <form class="form-horizontal" action="" method="post" enctype="multipart/form-data">
                      <div class="form-group">
                          <label for="inputFile" class="control-label col-xs-2">Select Image</label>
                          <div class="col-xs-10">
                              <input type="file" class="form-control" name="photo" id="inputFile">
                          </div>
                      </div>                   
                    
                      <div class="form-group">
                          <div class="col-xs-offset-2 col-xs-10">
                              <button type="submit" name="submit" class="btn btn-primary">Upload</button>
                          </div>
                      </div>
                  </form>
                  </div>
                </div>
            </div> 
           </div>
         </div>
      </div>
    <div class="container">
      <div class="row">
        <div class="col-md-8 col-md-offset-2">
        <?php
          $select ="SELECT * FROM fileupload";
          $result = mysqli_query($conn,$select);
        ?>
            <table class="table mytable">
              <tr>
                <th>SHOW IMAGE</th>
                 <th>REMOVE IMAGE</th>
              </tr>
                  <?php
                    if($result){
                        while($row=mysqli_fetch_array($result)){?>
                        <tr>
                            <td width="150">
                            <img src="<?php echo $row['image'];?>" alt="">
                            
                            </td>
                            <td>
                                <p><a href="delete.php?id=<?php echo $row['id'];?>">Remove</a></p>
                            </td>
                         
                        </tr>
                        
                     <?php       
                        }
                    }
                ?>
            </table>
        </div>
      </div>
    </div>
 <?php include "inc/footer.php";?>