
<?php
	if (isset($_POST['submit'])) {

			    $file_name   =  $_FILES["photo"]["name"];			 
			    $file_size 	 =  $_FILES["photo"]["size"];
			    $file_tmp    =  $_FILES["photo"]["tmp_name"];

			     $permited  = array('jpg', 'jpeg', 'png', 'gif'); 

			     $div 	   = explode(".", $file_name);
			     $file_ext = strtolower(end($div));
			     $unique_image = substr(md5(time()), 0, 5).'.'.$file_ext;
			     $uploaded_image = "uploads/".$unique_image;

			     if (empty($file_name)) {
		           echo "<span class='error'>Please Select any Image !</span>";
		          }elseif ($file_size >1048567) {
		           echo "<span class='error'>Image Size should be less then 1MB!
		           </span>";
		          } elseif (in_array($file_ext, $permited) === false) {
		           echo "<span class='error'>You can upload only:-"
		           .implode(', ', $permited)."</span>";
		          } else{

			    move_uploaded_file($file_tmp, $uploaded_image);
			    echo "<span class='text-success'>file upload successfully</span>";
			}
		
	}
 
?>





<!DOCTYPE html>
<html>
<head>
	<title></title>
	<style>
		.myform {
		    width: 650px;
		    margin: 0 auto;
		    border: 2px solid #00BCD4;
		    padding: 51px;
		    background: yellowgreen;
		}
		.error{
			color: red;
		}
		.success{
			background:  yellowgreen;
			padding: 5px;
		}
	</style>

</head>
<body>
	<div class="myform">
		<form action="" method="post" enctype="multipart/form-data">
			<table>
				<tr>
					<td>Select Image</td>
					<td>:</td>
					<td><input type="file" name="photo"></td>
				</tr>
				<tr>
					<td></td>
					<td></td>
					<td><input type="submit" name="submit" value="Upload"></td>
				</tr>
			</table>
		</form>
	</div>
</body>
</html>