<h3>Example 1 </h3>
<?php

for ($i = 1; $i <= 10; $i++) {
    echo $i;
}

?>
<h3>Example 2</h3>

<?php 
for ($i = 1; ; $i++) {
    if ($i > 10) {
        break;
    }
    echo $i;
}

 ?>


<h3>Example 3 </h3>

<?php 
$i = 1;
for (; ; ) {
    if ($i > 10) {
        break;
    }
    echo $i;
    $i++;
}

 ?>

<h3>Example 4</h3>

<?php 
        for ($i = 1, $j = 0; $i <= 10; $j += $i, print $i, $i++);
 ?>
