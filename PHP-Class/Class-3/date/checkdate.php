<?php



echo "April 30, 2010: ".(checkdate(4, 30, 2010) ? 'Valid' : 'Invalid');
// Returns false, because April only has 30 days
echo "<br/>";
echo "February 29, 2012: ".(checkdate(02, 29, 2012) ? 'Valid' : 'Invalid');
// Returns true, because 2012 is a leap year
echo "<br/>";
echo "February 29, 2012: ".(checkdate(02, 29, 2012) ? 'Valid' : 'Invalid');
// Returns false, because 2011 is not a leap year

?>