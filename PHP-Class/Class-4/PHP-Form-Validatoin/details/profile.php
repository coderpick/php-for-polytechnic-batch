<?php 
  session_start();
 ?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Profile</title>
	<link rel="stylesheet" href="style.css">
</head>
<body>
	<div class="wrapper">
		<h1>Your Details</h1>
		<div class="img">
			<img src='images/<?php echo $_SESSION['img']; ?>'>
		</div>
		<div class="content">
			<h2> <?php echo $_SESSION['fname']; ?> <?php echo $_SESSION['lname']; ?></h2>
			<p>Email: <?php echo $_SESSION['email']; ?></p>
			<p>Password: <?php echo substr(md5(isset($_POST['password'])),0,10); ?></p>
			<p>Date of Birth:- <?php echo $_SESSION['day']; ?>/<?php echo $_SESSION['month']; ?>/<?php echo $_SESSION['year']; ?></p>
			<p>You are a <?php echo $_SESSION['gender']; ?></p>
			<p>Your city <?php echo $_SESSION['city']; ?></p>
			<p>Your address:- <?php echo $_SESSION['address']; ?></p>
		</div>
	</div>
</body>
</html>