<?php
	session_start();
	if (isset($_POST['submit'])) {
		$fname		= $_POST['fname'];
		$lname		= $_POST['lname'];
		$email		= $_POST['email'];
		$pass		= $_POST['pass'];
		$day		= @$_POST['day'];
		$month		= @$_POST['month'];
		$year		= @$_POST['year'];
		$gender 	= $_POST['gender'];
		// image upload
		$filename 	= $_FILES['image']['name'];
		$filesize 	= $_FILES['image']['size'];
		$filedir  	= $_FILES['image']['tmp_name'];
		$div 	  	= explode('.', $filename);
		$ext 		= strtolower(end($div));
		$unique_img = substr(md5(time()),0,10).".".$ext;
		$upload 	= "details/images/".$unique_img;
		$permited 	= array('jpg','png','jpeg','gif');
		//end image upload
		$city 		= @$_POST['city'];
		$address	= $_POST['address'];

		$error 		= array();
		
		if ( $fname==NULL ) {
			$error['fname'] = 'First name is Blank'; 
		}
		if ( $lname==NULL ) {
			$error['lname'] = 'Last name is Blank'; 
		}
		if ( $email==NULL ) {
			$error['email'] = 'Email address is blank';
		}
		if ( $pass==NULL ) {
			$error['pass'] = 'Password is blank';
		}elseif ( strlen($pass) <= 5 ) {
			$error['pass']	= 'Password must be more more than 5 charecter';
		}
		if (!isset($_POST['day'])) {
			$error['day'] = 'Please select date of birth';
		}elseif (!isset($_POST['month'])) {
			$error['day'] = 'Please select date of birth';
		}elseif (!isset($_POST['year'])) {
			$error['day'] = 'Please select date of birth';
		}
		//image uploader
		if ($filename==NULL) {
			$error['img'] = "Please select any image";
		}elseif ($filesize>1024*1024){
			$error['img'] = "Image should be less than 1 MB</p>";
		}elseif (in_array($ext, $permited)==false) {
			$error['img'] = "You can uploaded only:-" .implode(',', $permited);
		}else {
			move_uploaded_file($filedir, $upload);
		}
		//end image uploader
		if (!isset($_POST['city'])) {
			$error['city'] = 'Select your city';
		}
		if ($address==NULL) {
			$error['address'] = 'Enter your address here';
		}else {
			$_SESSION['fname']		= $fname;
			$_SESSION['lname']		= $lname;
			$_SESSION['email']		= $email;
			$_SESSION['pass']		= $pass;
			$_SESSION['day']		= $day;
			$_SESSION['month']		= $month;
			$_SESSION['year']		= $year;
			$_SESSION['gender']		= $gender;
			$_SESSION['city']		= $city;
			$_SESSION['address']	= $address;
			$_SESSION['img']		= $unique_img;
			header('location:details/profile.php');

		}

	}
?>



<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Regitration Form</title>
	<link rel="stylesheet" href="style.css">
	<link href="https://fonts.googleapis.com/css?family=Lobster" rel="stylesheet">
</head>
<body>
	<div class="wrapper">
		<h2>Registration Form</h2>
		<form action="" method="post" enctype="multipart/form-data">
			<label for="fname">First Name</label>
			<input type="text" id="fname" name="fname" placeholder="Enter your first name">
			<p class="error"><?php if(isset($error['fname'])){ echo $error['fname']; } ?></p>
			<label for="lname">Last Name</label>
			<input type="text" id="lname" name="lname" placeholder="Enter your last name">
			<p class="error"><?php if(isset($error['lname'])){ echo $error['lname']; } ?></p>			
			<label for="email">Email Address</label>
			<input type="email" id="email" name="email" placeholder="Enter your email address">
			<p class="error"><?php if(isset($error['email'])){ echo $error['email']; } ?></p>			
			<label for="pass">Password</label>
			<input type="password" id="pass" name="pass" placeholder="Enter your password">
			<p class="error"><?php if(isset($error['pass'])){ echo $error['pass']; } ?></p>
			<p>Date of Birth</p>
			<select name="day">
				<option selected disabled>Day</option>
				<?php for ($i=1; $i <= 30 ; $i++) { ?>
					<option value="<?php echo $i; ?>"><?php echo $i; ?></option>
				<?php	} ?>
			</select>
			<select name="month">
				<option disabled selected>Month</option>
				<?php for ($i=1; $i <= 12 ; $i++) { ?>
					<option value="<?php echo $i; ?>"><?php echo $i; ?></option>
				<?php	} ?>
			</select>
			<select name="year">
				<option disabled selected>Year</option>
				<?php for ($i=1990; $i <= 2017 ; $i++) { ?>
					<option value="<?php echo $i; ?>"><?php echo $i; ?></option>
				<?php	} ?>
			</select>
			<p class="error"><?php if(isset($error['day'])) { echo $error['day'];} ?></p>
			<p>Gender</p>
			<input type="radio" name="gender" value="Male" id="gen" checked> <label for="gen">Male</label>
  			<input type="radio" name="gender" value="Female" id="fgen"> <label for="fgen">Female</label>
  			<p>Upload your profile picture</p>
  			<input type="file" name="image">
  			<p class="error"><?php if(isset($error['img'])){ echo $error['img']; } ?></p>
  			<p>Choose you City</p>
  			<select name="city">
  				<option selected disabled>Select</option>
  				<option value="Dhaka">Dhaka</option>
  				<option value="Narayanganj">Narayanganj</option>
  				<option value="Pabna">Pabna</option>
  				<option value="Rajshahi">Rajshahi</option>
  			</select>
  			<p class="error"><?php if(isset($error['city'])){ echo $error['city']; } ?></p>
			<p><label for="address">Address</label><p>
			<textarea name="address" id="address" placeholder="Enter your address"></textarea>
			<p class="error"><?php if(isset($error['address'])){ echo $error['address']; } ?></p>
			<input type="submit" name="submit" value="Register">
		</form>
	</div>
</body>
</html>