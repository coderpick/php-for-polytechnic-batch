<?php
    $nameError ="";
    $name ="";
    if (isset($_POST['submit']))
    {
        $name = test_input($_POST['name']);
        if (empty($name))
        {
            $nameError ="Name field is required";
        }

    }
    function test_input($data) {
      $data = trim($data);
      $data = stripslashes($data);
      $data = htmlspecialchars($data);
      return $data;
    }
?>
<style>
    .error{
        color: red;
        display: block;
    }
</style>
 <h2>Welcome <?php echo $name; ?></h2>
<hr>
<form action="" method="post">
    <p>
        <input type="text" name="name" placeholder="Enter Name">
        <span class="error">
            <?php
            if (isset($nameError)){
                echo $nameError;
            }

            ?></span>
    </p>
    <p><button type="submit" name="submit">Submit</button></p>
</form>