<!DOCTYPE html>
<html>
<body>
<h3>Syntax for single-line comments:</h3>
<?php
// This is a single-line comment

# This is also a single-line comment
?>
<h3>Syntax for multiple-line comments:</h3>
<?php
/*
This is a multiple-lines comment block
that spans over multiple
lines
*/
?>
<h3>Using comments to leave out parts of the code:</h3>
<?php
// You can also use comments to leave out parts of a code line
$x = 5 /* + 15 */ + 5;
echo $x;
?>


</body>
</html>