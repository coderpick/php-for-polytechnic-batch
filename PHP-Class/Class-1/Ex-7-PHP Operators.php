<h2>PHP Operators</h2>
<p>Operators are used to perform operations on variables and values.</p>

<p><b>PHP divides the operators in the following groups:</b></p>

<p>Arithmetic operators</p>
<p>Assignment operators</p>
<p>Comparison operators</p>
<p>Increment/Decrement operators</p>
<p>Logical operators</p>
<p>String operators</p>
<p>Array operators</p>
<p>Conditional assignment operators</p>