<!DOCTYPE html>
<html>
	<body>
		<h2>PHP Data Types</h2>
		<p>Variables can store data of different types, and different data types can do different things.</p>
		<h5>PHP supports the following data types:</h5>
		<p>1.String</p>
		<p>2.Integer</p>
		<p>3.Float (floating point numbers - also called double)</p>
		<p>4.Boolean</p>
		<p>5.Array</p>
		<p>6.Object</p>
		<p>7.NULL</p>
		<p>8.Resource</p>
		<h3>1. String</h3>
		<?php
		$x = "Hello Bangladesh!";
		$y = 'Hello Bangladesh!';
		echo $x;
		echo "<br>";
		echo $y;
		?>
		<h3>PHP Integer</h3>
		<p>	An integer data type is a non-decimal number between -2,147,483,648 and 2,147,483,647.</p>
		<h5>Rules for integers:</h5>
		<p>An integer must have at least one digit</p>
		<p>An integer must not have a decimal point</p>
		<p>An integer can be either positive or negative</p>
		<?php
		$x = 5985;
		var_dump($x);
		?>
		<h3>PHP Float</h3>
		<p>A float (floating point number) is a number with a decimal point or a number in exponential form.</p>
		<?php
		$x = 20.365;
		var_dump($x);
		?
		<h3>PHP Boolean</h3>
	   <p>A Boolean represents two possible states: TRUE or FALSE.</p>
		

		<p>$x = true;</p>
		<p>	$y = false;</p>
		   <p>	Booleans are often used in conditional testing.</p>
		   
		   <?php
			// Assign the value TRUE to a variable
			$show_error = True;
			var_dump($show_error);
			?>

			<h3>PHP Arrays</h3>
			<p>An array is a variable that can hold more than one value at a time. It is useful to aggregate a series of related items together, for example a set of country or city names.</p>
			<?php
			$colors = array("Red", "Green", "Blue");
			var_dump($colors);
			echo "<br>";
			
			$color_codes = array(
				"Red" => "#ff0000",
				"Green" => "#00ff00",
				"Blue" => "#0000ff"
			);
			var_dump($color_codes);
			?>

			<h3>PHP Object</h3>
			<p>An object is a data type which stores data and information on how to process that data.</p>
			<?php
			class Car {
				function Car() {
					$this->model = "BMW777";
				}
			}

			// create an object
			$herbie = new Car();

			// show object properties
			echo $herbie->model;
			?>
			<h3>PHP NULL Value</h3>
			<p>Null is a special data type which can have only one value: NULL.</p>
			<?php
			$x = "Hello Bangladesh!";
			$x = null;
			var_dump($x);
			?>
			<h3>PHP Resource</h3>
			<p>The special resource type is not an actual data type. It is the storing of a reference to functions and resources external to PHP.</p>

			<?php
			// Open a file for reading
			$handle = fopen("note.txt", "r");
			var_dump($handle);
			echo "<br>";
			
			// Connect to MySQL database server with default setting
			$link = mysql_connect("localhost", "root", "");
			var_dump($link);
			?>
	</body>
</html>