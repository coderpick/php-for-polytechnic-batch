<!DOCTYPE html>
<html>
<body>

<h3>In PHP, a variable starts with the $ sign, followed by the name of the variable:</h3>

	<?php
	$txt = "Welcome PHP7!";
	$x = 5;
	$y = 10.5;
?>
	<h4>PHP Variables</h4>
	<p>
	A variable can have a short name (like x and y) or a more descriptive name (age, carname, total_volume).
	</p>
 <h4>Rules for PHP variables:</h4>

  <ol>
  	<li>A variable starts with the $ sign, followed by the name of the variable</li>
	<li>A variable name must start with a letter or the underscore character</li>
	<li>A variable name cannot start with a number</li>
	<li>A variable name can only contain alpha-numeric characters and underscores (A-z, 0-9, and _ )</li>
	<li>Variable names are case-sensitive ($age and $AGE are two different variables)</li>
  </ol>


  <h3>Output Variables</h3>
  <?php
		$txt = "PHP";
		echo "I love $txt!";
	?>
</body>
</html>