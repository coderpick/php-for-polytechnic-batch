<h3>PHP Constants</h3>
<p>A constant is an identifier (name) for a simple value. The value cannot be changed during the script.</p>

<h4>Syntax</h4>
<p><b>define(name, value, case-insensitive)</b></p>