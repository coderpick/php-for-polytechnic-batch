<!DOCTYPE html>
<html>
<body>

  <h3>PHP Variables Scope</h3>
  	<p>In PHP, variables can be declared anywhere in the script.</p>

	<p>The scope of a variable is the part of the script where the variable can be referenced/used.</p>

	<p>PHP has three different variable scopes:</p>

		<h5>1. local</h5>
		<h5>2. global</h5>
		<h5>3. static</h5>

  	<h3>Variable with global scope:</h3>		
  	<?php
	$x = 5; // global scope

	function myTest() {
	    // using x inside this function will generate an error
	    echo "<p>Variable x inside function is: $x</p>";
	}
	myTest();

	echo "<p>Variable x outside function is: $x</p>";
	?>
	<h3>A variable declared within a function has a LOCAL SCOPE and can only be accessed within that function:</h3>
		<?php
		function myTest() {
		    $x = 5; // local scope
		    echo "<p>Variable x inside function is: $x</p>";
		}
		myTest();

		// using x outside the function will generate an error
		echo "<p>Variable x outside function is: $x</p>";
		?>

		<h3>PHP The global Keyword</h3>
		<p>The global keyword is used to access a global variable from within a function.</p>

		<?php
			$x = 5;
			$y = 10;

			function myTest() {
			    global $x, $y;
			    $y = $x + $y;
			}

			myTest();
			echo $y; // outputs 15
		?>
		or
		<?php
			$x = 5;
			$y = 10;

			function myTest() {
			    $GLOBALS['y'] = $GLOBALS['x'] + $GLOBALS['y'];
			}

			myTest();
			echo $y; // outputs 15
			?>
	  <h3>PHP The static Keyword</h3>		
		<p>Normally, when a function is completed/executed, all of its variables are deleted. However, sometimes we want a local variable NOT to be deleted. We need it for a further job.</p>
		<?php
		function myTest() {
		    static $x = 0;
		    echo $x;
		    $x++;
		}

		myTest();
		myTest();
		myTest();
		?>
</body>
</html>